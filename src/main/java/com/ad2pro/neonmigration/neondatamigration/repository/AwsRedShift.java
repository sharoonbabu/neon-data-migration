package com.ad2pro.neonmigration.neondatamigration.repository;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Slf4j
public class AwsRedShift {

    private final  String dbURL = "jdbc:redshift://adserve.crdbsipbk16y.us-east-1.redshift.amazonaws.com:5439/adserve";
    private final  String userName = "redshiftadmin";
    private final  String userPassword = "Adserv3Adm!n";
    public Connection connection ;
    private Properties properties;

    public AwsRedShift() throws ClassNotFoundException {

            log.info("Loading jdbc redshit class");
            Class.forName("com.amazon.redshift.jdbc42.Driver");
            log.info("set properties for db");
            setProperties();
    }

    public  Connection getConnection() throws SQLException {
        log.info("Get connection");
        return DriverManager.getConnection(dbURL, properties);
    }

    public  void  closeConnection() throws SQLException {
        connection.close();
    }

    private void  setProperties(){
        Properties properties1 = new Properties();
        properties1.setProperty("user", userName);
        properties1.setProperty("password", userPassword);
        properties = properties1;
    }

}
