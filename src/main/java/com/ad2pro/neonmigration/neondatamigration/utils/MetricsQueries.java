package com.ad2pro.neonmigration.neondatamigration.utils;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

public class MetricsQueries {

    private MetricsQueries(){}

    public static final String CLICK_QUERY  = "select * from asset_track_clicks where " +
            "atc_ref_id = 'jdx1gygp08wsk0p2pj98' and atc_lineitem_id != '0' limit 1";

    public static final String IMPRESSION_QUERY = "select * from asset_track_impressions where " +
            "ati_ref_id = 'jdx1gygp08wsk0p2pj98' and ati_lineitem_id != '0' limit 1";

    public static final String VIEWABLE_IMPRESSION_QUERY = "select * from asset_track_viewability where " +
            "atv_ref_id = 'jdx1gygp08wsk0p2pj98' and atv_lineitem_id != '0' limit 1";

    public static final String VIDEO_QUERY = "select * from asset_track_video_played2 where " +
            "atvp_ref_id = 'jdx1gygp08wsk0p2pj98' and atvp_lineitem_id != '0' limit 1";

    public static final String INTERACTED_QUERY = "select * from asset_ad_tag_user_interacted where " +
            "atua_ref_id = 'jdx1gygp08wsk0p2pj98' and atua_lineitem_id != '0' limit 1";

    public static final String USER_ACTIVITY = "select * from asset_ad_tag_user_activity where " +
            "atua_ref_id = 'jdx1gygp08wsk0p2pj98' and atua_lineitem_id != '0' limit 1";

    public  static final String MULTI_IMPRESSION = "select * from asset_track_multiimpression where atm_lineitem_id!=0 " +
            "and atm_ref_id = 'jdx1gygp08wsk0p2pj98' limit 1";

}
