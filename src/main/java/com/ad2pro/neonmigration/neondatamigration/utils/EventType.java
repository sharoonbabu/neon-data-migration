package com.ad2pro.neonmigration.neondatamigration.utils;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

public enum EventType {
    IMPRESSION,
    CLICK,
    VIDEO,
    VIEWABLEIMPRESSION,
    MULTIIMPRESSION
}
