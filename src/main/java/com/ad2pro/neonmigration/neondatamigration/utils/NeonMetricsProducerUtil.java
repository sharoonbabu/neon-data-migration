package com.ad2pro.neonmigration.neondatamigration.utils;

import com.ad2pro.neonmigration.neondatamigration.models.*;
import com.ad2pro.neonmigration.neondatamigration.producer.MetricsProducer;
import com.ad2pro.reporting.producer.api.IReportingProducer;
import com.ad2pro.reporting.producer.exception.ProducerException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;


/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Slf4j
public class NeonMetricsProducerUtil {


    private NeonMetricsProducerUtil(){}

    public static void  produceClicks(IReportingProducer reportingProducer, ResultSet clickResultSet) throws SQLException, JsonProcessingException, ProducerException {

        log.info("Start producing click metrics");
        ObjectMapper objectMapper = new ObjectMapper();

        while (clickResultSet.next()) {
            Map<String, String> event = new HashMap<>();
            Clicks clicks = new Clicks();
            clicks.setAtc_ref_id(clickResultSet.getString("atc_ref_id"));
            clicks.setAtc_ip(clickResultSet.getString("atc_ip"));
            clicks.setAtc_time(clickResultSet.getString("atc_time"));
            clicks.setAtc_lineitem_id(clickResultSet.getString("atc_lineitem_id"));
            clicks.setAtc_order_id(clickResultSet.getString("atc_order_id"));
            clicks.setAtc_creative_id(clickResultSet.getString("atc_creative_id"));
            clicks.setAtc_cookie_id(clickResultSet.getString("atc_cookie_id"));
            clicks.setAtc_referer(clickResultSet.getString("atc_referer"));

            event.put("data", objectMapper.writeValueAsString(clicks));
            event.put("eventType",EventType.CLICK.toString());
            //log.info("click row {}",  clicks.toString());

            log.info("click event {}", event);

            try {
                MetricsProducer.produceEventToKafka(reportingProducer, objectMapper.writeValueAsString(event),clickResultSet.getString("atc_ref_id") );
            } catch (ProducerException e) {
                log.error("Producer exception {} occurred while producing click metrics", e);
                throw  new ProducerException(e);
            }
        }
        log.info("Produced all click events successfully!");

    }

    public static void produceImpressions(IReportingProducer reportingProducer,ResultSet impressionsResultSet) throws SQLException, IOException, ProducerException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();


       while (impressionsResultSet.next()) {

           log.info("impression ati_ref_id {} ", impressionsResultSet.getString("ati_ref_id"));
           String os = impressionsResultSet.getString("ati_os").replaceAll("/", "").replaceAll("; US", "");

           log.info("os {}", os);

           JSONObject obj = new JSONObject(os);
           log.info("{}:{}:{}",obj.get("architecture"),obj.get("family"), obj.get("version"));
           OsPojo osPojo = new OsPojo();
           osPojo.setArchitecture(String.valueOf(obj.get("architecture")));
           osPojo.setFamily(obj.get("family").toString());
           osPojo.setVersion(String.valueOf(obj.get("version")));

           Map<String, String> event = new HashMap<>();
           Impression impression = new Impression();
           impression.setAti_ref_id(impressionsResultSet.getString("ati_ref_id"));
           impression.setAti_ip(impressionsResultSet.getString("ati_ip"));
           impression.setAti_time(impressionsResultSet.getString("ati_time"));
           impression.setAti_lineitem_id(impressionsResultSet.getString("ati_lineitem_id"));
           impression.setAti_order_id(impressionsResultSet.getString("ati_order_id"));
           impression.setAti_creative_id(impressionsResultSet.getString("ati_creative_id"));
           impression.setAti_cookie_id(impressionsResultSet.getString("ati_cookie_id"));
           impression.setAti_referer(impressionsResultSet.getString("ati_referer"));
           impression.setAti_device(impressionsResultSet.getString("ati_device"));
           impression.setAti_os(objectMapper.writeValueAsString(osPojo));
           /*try{
               impression.setAti_os(objectMapper.writeValueAsString(osPojo));
           }catch (Exception e) {
               log.info("Exception {} occurred while  setting os", e.getMessage());
               OsPojo osPojo1 = new OsPojo();
               impression.setAti_os(objectMapper.writeValueAsString(osPojo1));
           }*/
           impression.setAti_browser(impressionsResultSet.getString("ati_browser"));
           impression.setAti_manufacturer(impressionsResultSet.getString("ati_manufacturer"));
           impression.setAti_browser_version(impressionsResultSet.getString("ati_browser_version"));

           event.put("eventType",EventType.IMPRESSION.toString());
           event.put("data", objectMapper.writeValueAsString(impression));

           try {
               MetricsProducer.produceEventToKafka(reportingProducer, objectMapper.writeValueAsString(event),
                       impressionsResultSet.getString("ati_ref_id") );
           } catch (ProducerException e) {
               log.error("Producer exception {} occurred while producing impression metrics", e);
               throw  new ProducerException(e);
           }

           log.info("impression row {}", objectMapper.writeValueAsString(event));
       }

    }

    public static void produceViewableImpressions(IReportingProducer reportingProducer,ResultSet resultSet) throws SQLException, JsonProcessingException, ProducerException {

        ObjectMapper objectMapper = new ObjectMapper();

        while (resultSet.next()){

            Map<String, String> event = new HashMap<>();
            ViewableImpression viewableImpression = new ViewableImpression();
            viewableImpression.setAtv_ref_id(resultSet.getString("atv_ref_id"));
            viewableImpression.setAtv_ip(resultSet.getString("atv_ip"));
            viewableImpression.setAtv_time(resultSet.getString("atv_time"));
            viewableImpression.setAtv_lineitem_id(resultSet.getString("atv_lineitem_id"));
            viewableImpression.setAtv_order_id(resultSet.getString("atv_order_id"));
            viewableImpression.setAtv_creative_id(resultSet.getString("atv_creative_id"));
            viewableImpression.setAtv_cookie_id(resultSet.getString("atv_cookie_id"));
            viewableImpression.setAtv_referer(resultSet.getString("atv_referer"));
            log.info("Viewable impression {}", viewableImpression.toString());

            event.put("eventType",EventType.VIEWABLEIMPRESSION.toString());
            event.put("data", objectMapper.writeValueAsString(viewableImpression));
            try {
                MetricsProducer.produceEventToKafka(reportingProducer, objectMapper.writeValueAsString(event),
                        resultSet.getString("atv_ref_id") );
            } catch (ProducerException e) {
                log.error("Producer exception {} occurred while producing viewable impression metrics", e);
                throw  new ProducerException(e);
            }

        }
    }

    public static  void produceVideoMetrics(IReportingProducer reportingProducer,ResultSet resultSet) throws SQLException, JsonProcessingException, ProducerException {

        ObjectMapper objectMapper = new ObjectMapper();

        while (resultSet.next()) {

            Map<String, String> event = new HashMap<>();
            Video video = new Video();
            video.setAtvp_ref_id(resultSet.getString("atvp_ref_id"));
            video.setAtvp_ip(resultSet.getString("atvp_ip"));
            video.setAtvp_time(resultSet.getString("atvp_time"));
            video.setAtvp_lineitem_id(resultSet.getString("atvp_lineitem_id"));
            video.setAtvp_order_id(resultSet.getString("atvp_order_id"));
            video.setAtvp_creative_id(resultSet.getString("atvp_creative_id"));
            video.setAtvp_cookie_id(resultSet.getString("atvp_cookie_id"));
            video.setAtvp_duration(resultSet.getString("atvp_duration"));
            video.setAtvp_repeat(resultSet.getString("atvp_repeat"));
            video.setAtvp_referer(resultSet.getString("atvp_referer"));
            log.info("Video metrics {}", video.toString());

            event.put("eventType",EventType.VIDEO.toString());
            event.put("data", objectMapper.writeValueAsString(video));
            try {
                MetricsProducer.produceEventToKafka(reportingProducer, objectMapper.writeValueAsString(event),
                        resultSet.getString("atvp_ref_id") );
            } catch (ProducerException e) {

                log.error("Producer exception {} occurred while producing video metrics", e);
                throw  new ProducerException(e);
            }

        }
    }

    public static void produceInteractedMetrics(IReportingProducer reportingProducer,ResultSet resultSet) throws SQLException, ProducerException, JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();

        while (resultSet.next()) {
            Map<String, String> event = new HashMap<>();

            Interacted interacted = new Interacted();
            interacted.setAtua_ref_id(resultSet.getString("atua_ref_id"));
            interacted.setAtua_ip(resultSet.getString("atua_ip"));
            interacted.setAtua_time(resultSet.getString("atua_time"));
            interacted.setAtua_lineitem_id(resultSet.getString("atua_lineitem_id"));
            interacted.setAtua_order_id(resultSet.getString("atua_order_id"));
            interacted.setAtua_creative_id(resultSet.getString("atua_creative_id"));
            interacted.setAtua_cookie_id(resultSet.getString("atua_cookie_id"));
            interacted.setAtua_event(resultSet.getString("atua_event"));
            interacted.setAtua_event_value(resultSet.getString("atua_event_value"));
            interacted.setAtua_referer(resultSet.getString("atua_referer"));
            log.info("Interacted Metrics {}", interacted.toString());

            /**
             * event columns name:atua_event
             * values:HIDE,'SHOW','FULLSCREEN','FULLSCREENEXIT','SCROLLON','SCROLLOFF',INTERACTEDIMPR,INTERACTED
             */

            event.put("eventType",resultSet.getString("atua_event"));
            event.put("data", objectMapper.writeValueAsString(interacted));

            try {
                MetricsProducer.produceEventToKafka(reportingProducer, objectMapper.writeValueAsString(event),
                        resultSet.getString("atua_ref_id") );
            } catch (ProducerException e) {

                log.error("Producer exception {} occurred while producing interacted metrics", e);
                throw  new ProducerException(e);
            }

        }
    }

    public static void produceUserActivityMetrics(IReportingProducer reportingProducer,ResultSet resultSet) throws SQLException, JsonProcessingException, ProducerException {

        ObjectMapper objectMapper = new ObjectMapper();

        while (resultSet.next()){

            Map<String, String> event = new HashMap<>();
            UserActivity userActivity = new UserActivity();
            userActivity.setAtua_ref_id(resultSet.getString("atua_ref_id"));
            userActivity.setAtua_ip(resultSet.getString("atua_ip"));
            userActivity.setAtua_time(resultSet.getString("atua_time"));
            userActivity.setAtua_lineitem_id(resultSet.getString("atua_lineitem_id"));
            userActivity.setAtua_order_id(resultSet.getString("atua_order_id"));
            userActivity.setAtua_creative_id(resultSet.getString("atua_creative_id"));
            userActivity.setAtua_cookie_id(resultSet.getString("atua_cookie_id"));
            userActivity.setAtua_event(resultSet.getString("atua_event"));
            userActivity.setAtua_referer(resultSet.getString("atua_referer"));

            /**
             * event columns name:atua_event
             * values:VIEWED,DWELLED,PAUSED,PLAYED,MUTEDON,MUTEDOFF,SHOW,HIDE,SCROLLON,SCROLLOFF
             */

            event.put("eventType",resultSet.getString("atua_event"));
            event.put("data", objectMapper.writeValueAsString(userActivity));

            try {
                MetricsProducer.produceEventToKafka(reportingProducer, objectMapper.writeValueAsString(event),
                        resultSet.getString("atua_ref_id"));
            } catch (ProducerException e) {
                log.error("Producer exception {} occurred while producing user activity metrics metrics", e);
                throw  new ProducerException(e);
            }
        }
    }

    public static void produceMultiImpression(IReportingProducer reportingProducer,ResultSet resultSet) throws SQLException, JsonProcessingException, ProducerException {


        ObjectMapper objectMapper = new ObjectMapper();

        while (resultSet.next()) {

            String os = resultSet.getString("atm_os").replaceAll("/", "").replaceAll("; US","");

            JSONObject obj = new JSONObject(os);
            OsPojo osPojo = new OsPojo();
            osPojo.setArchitecture(String.valueOf(obj.get("architecture")));
            osPojo.setFamily(obj.get("family").toString());
            osPojo.setVersion(String.valueOf(obj.get("version")));

            Map<String, String> event = new HashMap<>();
            MultiImpression multiImpression = new MultiImpression();
            multiImpression.setAtm_ref_id(resultSet.getString("atm_ref_id"));
            multiImpression.setAtm_date(resultSet.getString("atm_date"));
            multiImpression.setAtm_creative_id(resultSet.getString("atm_creative_id"));
            multiImpression.setAtm_lineitem_id(resultSet.getString("atm_lineitem_id"));
            multiImpression.setAtm_order_id(resultSet.getString("atm_order_id"));
            multiImpression.setAtm_impressions(resultSet.getString("atm_impressions"));
            multiImpression.setAtm_cookie_id(resultSet.getString("atm_cookie_id"));
            multiImpression.setAtm_referer(resultSet.getString("atm_referer"));
            multiImpression.setAtm_category(resultSet.getString("atm_category"));
            multiImpression.setAtm_trackaction(resultSet.getString("atm_trackaction"));
            multiImpression.setAtm_device(resultSet.getString("atm_device"));
            multiImpression.setAtm_os(objectMapper.writeValueAsString(osPojo));
            /*try{
                multiImpression.setAtm_os(objectMapper.writeValueAsString(osPojo));
            }catch (Exception e) {
                log.info("Exception {} occurred while  setting os", e.getMessage());
                OsPojo osPojo1 = new OsPojo();
                multiImpression.setAtm_os(objectMapper.writeValueAsString(osPojo1));
            }*/
            multiImpression.setAtm_browser(resultSet.getString("atm_browser"));
            multiImpression.setAtm_manufacturer(resultSet.getString("atm_manufacturer"));
            multiImpression.setAtm_browser_version(resultSet.getString("atm_browser_version"));
            multiImpression.setAtm_ip(resultSet.getString("atm_ip"));

            event.put("eventType",EventType.MULTIIMPRESSION.toString());
            event.put("data", objectMapper.writeValueAsString(multiImpression));

              try {
                MetricsProducer.produceEventToKafka(reportingProducer, objectMapper.writeValueAsString(event),
                        resultSet.getString("atm_ref_id"));
            } catch (ProducerException e) {
                log.error("Producer exception {} occurred while producing multi impression metrics", e);
                throw  new ProducerException(e);
            }

        }

    }

}
