package com.ad2pro.neonmigration.neondatamigration.utils;

public enum JobStatus {

    QUEUED,
    SUCCESS,
    FAILED
}
