package com.ad2pro.neonmigration.neondatamigration.scheduler;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Slf4j
public class QueryHelper {

    private Connection connection;

    QueryHelper(Connection connection){
        this.connection = connection;
    }

  /*  private ResultSet executeQuery(String sqlQuery) throws SQLException {

        Statement statement = connection.createStatement();
        return statement.executeQuery(sqlQuery);
    }*/

    public ResultSet fetchMetrics(String sqlQuery) throws SQLException {

        Statement statement = connection.createStatement();
        return statement.executeQuery(sqlQuery);

    }
}
