package com.ad2pro.neonmigration.neondatamigration.scheduler;
import com.ad2pro.neonmigration.neondatamigration.config.AppConfig;
import com.ad2pro.neonmigration.neondatamigration.models.JobStatusTrack;
import com.ad2pro.neonmigration.neondatamigration.repository.AwsRedShift;
import com.ad2pro.neonmigration.neondatamigration.utils.BeanUtil;
import com.ad2pro.neonmigration.neondatamigration.utils.JobStatus;
import com.ad2pro.neonmigration.neondatamigration.utils.NeonMetricsProducerUtil;
import com.ad2pro.reporting.producer.api.IReportingProducer;
import com.ad2pro.reporting.producer.exception.ProducerException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;


/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Component
@Slf4j
public class NeonScheduler {


    //String[] args
    private NeonScheduler(){};
    //@Scheduled(fixedRate = 1000000000, initialDelay = 1000)
    public static void gerMetrics(String[] args){

        /*AppConfig appConfig = BeanUtil.getBean(AppConfig.class);
        Long corpId= 2000l;
        Long adServerId = 2000l;
        Map<String, String> lastQuery =
                getLastReportQueryDateTimeAndStatus(corpId,adServerId);

        if(lastQuery.get("lastReportStatus").equals(JobStatus.QUEUED.toString())) {
            log.info("Previously scheduled job still in queue");
        }
        else {
            neonAdTags.syncNeonAdTags(lastQuery.get("lastReportQueryDate"));
        }
        Map<String,String> dates = getQueryDates(lastQuery.get("lastReportQueryDate"));
        log.info("==================== {}", appConfig.getSchedulerTrackingFilePath());*/


        /*String orderId = args[0];*/
        //String fromDate = args[0].replaceAll("T", " ");
        //String toDate = args[1].replaceAll("T", " ");
        String refId = args[0];
        String corpId = args[1];
        String eventType = args[2];
        String refIds = refId.replace(",","','");


        String orderId = "2545471377";
       /* String fromDate = "2019-10-01 00:00:00";
        String toDate = "2019-10-30 23:59:59";*/
        log.info("collect command line arguments");
        //log.info("orderId:{}, fromData:{}, toData:{}", orderId, fromDate, toDate);

        try {

            AwsRedShift awsRedShift = new AwsRedShift();
            Connection connection = awsRedShift.getConnection();
            IReportingProducer reportingProducer = KafkaProducer.getInstance();
            log.info("Connection to redshift made successfully!");

            ResultSet resultSet;
            QueryHelper metricsHelper = new QueryHelper(connection);

            switch (eventType) {
                case "IMPRESSION" :
                    log.info("Start fetching metrics from redshift");
                    log.info("fetch impression");
                    //start impression
                    String impressionQuery = "select * from asset_track_impressions " +
                            "join asset_ad_tags_tegna on (aat_ref_no=ati_ref_id and aat_corp_id='"+corpId+"') " +
                            "where ati_ref_id in ('" + refIds + "') " +
                            //"ati_time>='"+fromDate+"' and " +
                            //"ati_time<='"+toDate+"' and  " +
                            "and ati_lineitem_id != '0' order by ati_time";
                    log.info("Impression query {}", impressionQuery);
                    resultSet = metricsHelper.fetchMetrics(impressionQuery);
                    log.info("Impression fetch success resultSet:{}", resultSet);
                    NeonMetricsProducerUtil.produceImpressions(reportingProducer, resultSet);
                    //end impression  polling here
                    break;
                case "CLICK" :
                    //start click polling
                    log.info("fetch Clicks");
                    String clickQuery = "select * from asset_track_clicks " +
                            "join asset_ad_tags_tegna on (aat_ref_no=atc_ref_id and aat_corp_id='"+corpId+"') " +
                            " where  atc_ref_id in ('" + refIds + "') "+
                            //"and  atc_time>= '"+fromDate+"' " +
                            //" and atc_time<='"+toDate+"'  " +
                            "and  atc_lineitem_id != '0' order by atc_time ";
                    log.info("clickQuery query {}", clickQuery);
                    resultSet = metricsHelper.fetchMetrics(clickQuery);
                    log.info("Clicks fetch success resultSet:{}", resultSet);
                    NeonMetricsProducerUtil.produceClicks(reportingProducer,resultSet);
                    //click polling end here
                    break;
                case "ATV" :
                    //start viewable impression here
                    log.info("Fetch viewable impression");
                    String viewableImpression = "select * from asset_track_viewability " +
                            "join asset_ad_tags_tegna on (aat_ref_no=atv_ref_id and aat_corp_id='"+corpId+"') " +
                            "where atv_ref_id in ('" + refIds + "') "+
                            //"and atv_time>='"+fromDate+"' " +
                            //"and atv_time<='"+toDate+"' " +
                            "and atv_lineitem_id != '0'  order by atv_time";
                    log.info("viewableImpression query {}", viewableImpression);
                    resultSet = metricsHelper.fetchMetrics(viewableImpression);
                    log.info("Fetch viewable impression success resultSet:{}", resultSet);
                    NeonMetricsProducerUtil.produceViewableImpressions(reportingProducer,resultSet);
                    //end viewable impression here
                    break;
                case "ATVP2" :
                    //video polling start here
                    log.info("Fetch video metrics");
                    String videoQuery = "select * from asset_track_video_played2 " +
                            "join asset_ad_tags_tegna on (aat_ref_no=atvp_ref_id and aat_corp_id='"+corpId+"') " +
                            "where atvp_ref_id in ('" + refIds + "') "+
                            //"and  atvp_time>='"+fromDate+"' " +
                            //"and atvp_time<='"+toDate+"' " +
                            " and  atvp_lineitem_id != '0' order by atvp_time ";
                    log.info("videoQuery query {}", videoQuery);
                    resultSet = metricsHelper.fetchMetrics(videoQuery);
                    log.info("Fetch video success resultSet:{}", resultSet);
                    NeonMetricsProducerUtil.produceVideoMetrics(reportingProducer,resultSet);
                    //video polling end here
                    break;
                case "UI" :
                    //interacted polling starts here
                    log.info("Fetch interacted metrics");
                    String interactedQuery = "select * from asset_ad_tag_user_interacted " +
                            "join asset_ad_tags_tegna on (aat_ref_no=atua_ref_id and aat_corp_id='"+corpId+"') " +
                            "where atua_ref_id in ('" + refIds + "') "+
                            //"and atua_time>='"+fromDate+"' " +
                            //"and atua_time<='"+toDate+"'  " +
                            "and  atua_lineitem_id != '0' order by atua_time ";
                    log.info("interactedQuery query {}", interactedQuery);
                    resultSet = metricsHelper.fetchMetrics(interactedQuery);
                    log.info("Fetch interacted metrics success resultSet:{}", resultSet);
                    NeonMetricsProducerUtil.produceInteractedMetrics(reportingProducer,resultSet);
                    //interacted polling end here
                    break;
                case "UA" :
                    //user activity polling start here
                    log.info("fetch user activity");
                    String userQuery = "select * from asset_ad_tag_user_activity " +
                            "join asset_ad_tags_tegna on (aat_ref_no=atua_ref_id and aat_corp_id='"+corpId+"') " +
                            "where atua_ref_id in ('" + refIds + "') "+
                            //"and atua_time>='"+fromDate+"' " +
                            //"and atua_time<='"+toDate+"' " +
                            "and  atua_lineitem_id != '0' order by atua_time ";
                    log.info("userQuery query {}", userQuery);
                    resultSet = metricsHelper.fetchMetrics(userQuery);
                    log.info("fetch user activity success, resultSet:{}", resultSet);
                    NeonMetricsProducerUtil.produceUserActivityMetrics(reportingProducer,resultSet);
                    //user activity polling end here
                    break;
                case "MULTIIMPR" :
                    //multi impression starts here
                    log.info("fetch multi impression");
                    String multiImpressoinQuery = "select * from asset_track_multiimpression " +
                            "join asset_ad_tags_tegna on (aat_ref_no=atm_ref_id and aat_corp_id='"+corpId+"') " +
                            "where atm_ref_id in ('" + refIds + "') "+
                            //"and atm_date>='"+fromDate+"' " +
                            //"and atm_date<='"+toDate+"' " +
                            "and atm_referer != 'undefined' " +
                            "and atm_ref_id != 'undefined' " +
                            "and atm_referer not like 'file:///var/%' " +
                            "and atm_lineitem_id !=0 order by atm_date";
                    log.info("multiImpressoinQuery query {}", multiImpressoinQuery);
                    resultSet = metricsHelper.fetchMetrics(multiImpressoinQuery);
                    log.info("fetch multi impression success, resultSet:{}", resultSet);
                    NeonMetricsProducerUtil.produceMultiImpression(reportingProducer,resultSet);
                    //multi impression end here
                    break;
            }

            log.info("Produced all the metrics successfully, close redshift jdbc4 connection");
        }
        catch (SQLException e) {
            log.error("Sql exception {} occurred while fetching clicks metrics", e);
        } catch (ClassNotFoundException e) {
            log.error("ClassNotFoundException {} exception occurred ",e);
        } catch (ProducerException e) {
            log.info("ProducerException exception occured ");
        }  catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*private static Map<String,String> getQueryDates(String lastReportQueryDate) {

        Map <String,String> dates = new HashMap<>();
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String currentDate = dateTime.format(formatter);
        log.info("current date {}", currentDate);
        if(lastReportQueryDate == null) {
            String fromDate = dateTime.minusMinutes(30).format(formatter);
            dates.put("fromDate", fromDate);
        }
        else dates.put("fromDate", lastReportQueryDate);
        dates.put("toDate", currentDate);
        return  dates;
    }*/

    /*public static Long jobScheduled(Long adServerId, Long corpId, JobStatus status, Integer attempt, String queryDate) throws Exception {

        try {
            AppConfig appConfig = BeanUtil.getBean(AppConfig.class);
            JobStatusTrack jobStatusTrack = new JobStatusTrack();
            jobStatusTrack.setAdserverId(adServerId);
            jobStatusTrack.setCorpId(corpId);
            jobStatusTrack.setStatus(status.toString());
            jobStatusTrack.setAttempt(attempt);
            jobStatusTrack.setQueriedDateTime(queryDate.replaceAll(" ", "T"));
                Long jobId = post(appConfig.getJobTrackingStatusUri(), jobStatusTrack);
            log.info("Generated scheduled jobId for corpId {} and adServerId {}", jobId, adServerId);
            return jobId;
        }
        catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }*/

    /*public static Long post(String uri, JobStatusTrack jobStatusTrack) throws Exception{
        log.info("Making Post http request for uri {} and payload {}", uri, jobStatusTrack);
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<JobStatusTrack> entity = new HttpEntity<>(jobStatusTrack, httpHeaders);
            ResponseEntity<Map> response = restTemplate.postForEntity(uri, entity, Map.class);
            Long jobId = Long.parseLong(String.valueOf(response.getBody().get("jobId")));
            return jobId;
        }
        catch (Exception e) {
            log.info("Exception occurred while making http post call for corpId {}, adServerId {}",
                    jobStatusTrack.getCorpId(), jobStatusTrack.getAdserverId());
            throw  new Exception(e);
        }
    }*/

    /*public static Map<String, String> getLastReportQueryDateTimeAndStatus(Long corpId, Long adServerId) {

        AppConfig appConfig = BeanUtil.getBean(AppConfig.class);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(appConfig.getLastDonwloadedReportDateUri());
        stringBuilder.append("?corpId=");
        stringBuilder.append(corpId);
        stringBuilder.append("&adServerId=");
        stringBuilder.append(adServerId);
        String uri = stringBuilder.toString();
        log.info("Reading last report query datetime for corpId {}, adServerId {}", corpId, adServerId);
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> response =  restTemplate.getForObject(uri, Map.class);
        String lastReportStatus = null;
        String lastReportQueryDate = null;
        if(response != null) {
            lastReportStatus = response.get("status");
            lastReportQueryDate = response.get("lastReportQueryDate");
            lastReportQueryDate = lastReportQueryDate.substring(0, lastReportQueryDate.length() - 2);//remove last 2 char,Ex:.0
        }
        Map<String, String> result = new HashMap<>();
        result.put("lastReportQueryDate", lastReportQueryDate);
        result.put("lastReportStatus", lastReportStatus);
        log.info("-----------------------------------");
        return result;
    }*/

}
