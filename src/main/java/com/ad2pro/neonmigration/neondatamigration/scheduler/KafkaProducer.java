package com.ad2pro.neonmigration.neondatamigration.scheduler;

import com.ad2pro.neonmigration.neondatamigration.config.AppConfig;
import com.ad2pro.neonmigration.neondatamigration.utils.BeanUtil;
import com.ad2pro.reporting.producer.api.ContinuousStreamInfo;
import com.ad2pro.reporting.producer.api.IReportingProducer;
import com.ad2pro.reporting.producer.impl.ReportingProducerFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Slf4j
public class KafkaProducer {

    public static IReportingProducer reportingProducer;

    private KafkaProducer(){}

    public static IReportingProducer getInstance() {

         if (reportingProducer  != null) return reportingProducer;
         synchronized (KafkaProducer.class){
             if(reportingProducer == null) {

                 ContinuousStreamInfo streamInfo = new ContinuousStreamInfo();
                 Map<String, Object> props = new HashMap<>();
                 ReportingProducerFactory reportingProducerFactory = ReportingProducerFactory.getInstance();

                 log.info("Get appConfig bean");
                 //AppConfig appConfig = BeanUtil.getBean(AppConfig.class);
                 log.info("Got appConfig bean");
                 //String topicName = appConfig.getNeonProducerName();
                 String topicName = "neon-events";
                 reportingProducer = reportingProducerFactory.getReportingProducer(topicName, streamInfo, props);
             }
         }
         return reportingProducer;
    }

}
