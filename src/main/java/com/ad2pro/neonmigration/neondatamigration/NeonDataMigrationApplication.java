package com.ad2pro.neonmigration.neondatamigration;


import com.ad2pro.neonmigration.neondatamigration.scheduler.NeonScheduler;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class NeonDataMigrationApplication   {

	public static void main(String[] args) {

		SpringApplication.run(NeonDataMigrationApplication.class, args);
		NeonScheduler.gerMetrics(args);
	}

	/*@Override
	public void run(ApplicationArguments args) throws Exception {

		NeonScheduler.gerMetrics(args);

	}*/
}
