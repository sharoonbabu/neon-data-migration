package com.ad2pro.neonmigration.neondatamigration.models;

import lombok.Data;

@Data
public class JobStatusTrack {

    private Long adserverId;
    private Long corpId;
    private String status;
    private Integer attempt;
    private String queriedDateTime;

}
