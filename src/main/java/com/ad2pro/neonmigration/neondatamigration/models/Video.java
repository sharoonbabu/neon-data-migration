package com.ad2pro.neonmigration.neondatamigration.models;

import lombok.Data;
import java.io.Serializable;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Data
public class Video implements Serializable {

    private static final long serialVersionUID = -191891999730860733L;

    private String atvp_ref_id;
    private String atvp_ip;
    private String atvp_time;
    private String atvp_lineitem_id;
    private String atvp_order_id;
    private String atvp_creative_id;
    private String atvp_cookie_id;
    private String atvp_duration;
    private String atvp_repeat;
    private String atvp_referer;

}
