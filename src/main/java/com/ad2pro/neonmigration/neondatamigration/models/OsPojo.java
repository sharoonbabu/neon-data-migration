package com.ad2pro.neonmigration.neondatamigration.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OsPojo implements Serializable {


    private static final long serialVersionUID = -319537802040758037L;

    private String architecture;
    private String family;
    private String version;
}
