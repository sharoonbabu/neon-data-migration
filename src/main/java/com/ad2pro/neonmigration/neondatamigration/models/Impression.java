package com.ad2pro.neonmigration.neondatamigration.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.io.Serializable;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Data
public class Impression implements Serializable {

    private static final long serialVersionUID = -2183287323397756847L;

    private String ati_ref_id;
    private String ati_ip;
    private String ati_time;
    private String ati_lineitem_id;
    private String ati_order_id;
    private String ati_creative_id;
    private String ati_cookie_id;
    private String ati_referer;
    private String ati_device;
    private String ati_os;
    private String ati_browser;
    private String ati_manufacturer;
    private String ati_browser_version;

    @Override
    public String toString(){

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
