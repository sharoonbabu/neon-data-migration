package com.ad2pro.neonmigration.neondatamigration.models;

import lombok.Data;
import java.io.Serializable;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Data
public class UserActivity implements Serializable {

    private static final long serialVersionUID = 8784187844553715592L;

    private String atua_ref_id;
    private String atua_ip;
    private String atua_time;
    private String atua_lineitem_id;
    private String atua_order_id;
    private String atua_creative_id;
    private String atua_cookie_id;
    private String atua_event;
    private String atua_referer;

}
