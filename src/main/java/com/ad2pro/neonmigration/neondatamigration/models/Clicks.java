package com.ad2pro.neonmigration.neondatamigration.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.io.Serializable;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Data
public class Clicks implements Serializable {

    private static final long serialVersionUID = 2051961686354231692L;

    private String atc_ref_id;
    private String atc_ip;
    private String atc_time;
    private String atc_lineitem_id;
    private String atc_order_id;
    private String atc_creative_id;
    private String atc_cookie_id;
    private String atc_referer;

    @Override
    public String toString(){

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
