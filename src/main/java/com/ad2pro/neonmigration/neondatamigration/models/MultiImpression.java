package com.ad2pro.neonmigration.neondatamigration.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class MultiImpression implements Serializable {

    private static final long serialVersionUID = 1852068698441921911L;

    private String atm_ref_id;
    private String atm_date;
    private String atm_creative_id;
    private String atm_lineitem_id;
    private String atm_order_id;
    private String atm_impressions;
    private String atm_cookie_id;
    private String atm_referer;
    private String atm_category;
    private String atm_trackaction;
    private String atm_device;
    private String atm_os;
    private String atm_browser;
    private String atm_manufacturer;
    private String atm_browser_version;
    private String atm_ip;

}
