package com.ad2pro.neonmigration.neondatamigration.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import java.io.Serializable;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Data
public class ViewableImpression  implements Serializable {

    private static final long serialVersionUID = 1274073587230260741L;

    private String atv_ref_id;
    private String atv_ip;
    private String atv_time;
    private String atv_lineitem_id;
    private String atv_order_id;
    private String atv_creative_id;
    private String atv_cookie_id;
    private String atv_referer;

    @Override
    public String toString() {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
