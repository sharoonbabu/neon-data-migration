package com.ad2pro.neonmigration.neondatamigration.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;


/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

@Configuration
@Data
public class AppConfig {

    @Value("${neon.producer.name:#{null}}")
    private String neonProducerName;

    @Value("${scheduler.tracking.filePath}")
    private String schedulerTrackingFilePath;

    @Value("${api.lastdonwloaded.reportdate}")
    private String lastDonwloadedReportDateUri;


}
