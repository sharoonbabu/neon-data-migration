package com.ad2pro.neonmigration.neondatamigration.producer;

import com.ad2pro.reporting.producer.api.IReportingProducer;
import com.ad2pro.reporting.producer.exception.ProducerException;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Author:Umakant Mane(umakant.b@2adpro.com)
 * Date:2019/Sep/26
 * */

public class MetricsProducer {

    public static void produceEventToKafka(IReportingProducer reportingProducer, String event, String refId) throws ProducerException {
        LocalDateTime now = LocalDateTime.now();
        reportingProducer.produceEvent(now.toEpochSecond(ZoneOffset.UTC), event, refId);
    }

}
